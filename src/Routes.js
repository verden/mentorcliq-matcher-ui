import React from 'react';
import {Redirect, Switch} from 'react-router-dom';

import {RouteWithLayout} from './components';
import {Main as MainLayout, Minimal as MinimalLayout} from './layouts';

import {
    EmployeeList as EmployeeListView,
    MatcherList as MatcherListView,
    Matching as MatchingView,
    NotFound as NotFoundView
} from './views';

const Routes = () => {
    return (
        <Switch>
            <Redirect
                exact
                from="/"
                to="/employees"
            />
            <RouteWithLayout
                component={EmployeeListView}
                exact
                layout={MainLayout}
                path="/employees"
            />
            <RouteWithLayout
                component={MatchingView}
                exact
                layout={MainLayout}
                path="/matching"
            />
            <RouteWithLayout
                component={MatcherListView}
                exact
                layout={MainLayout}
                path="/matchers"
            />
            <RouteWithLayout
                component={NotFoundView}
                exact
                layout={MinimalLayout}
                path="/not-found"
            />
            <Redirect to="/not-found"/>
        </Switch>
    );
};

export default Routes;
