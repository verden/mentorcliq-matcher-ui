import uuid from 'uuid/v1';

export default [
    {
        id: uuid(),
        name: 'matcher 1 name',
        alg: "alg",
        parameter: "param"
    },
    {
        id: uuid(),
        name: 'matcher 2 name',
        alg: "alg",
        parameter: "param"
    },

    {
        id: uuid(),
        name: 'matcher 3 name',
        alg: "alg",
        parameter: "param"
    },

    {
        id: uuid(),
        name: 'matcher 4 name',
        alg: "alg",
        parameter: "param"
    }
];
