import {makeStyles} from "@material-ui/styles";
import clsx from "clsx";
import {Button, Card, CardContent, CardHeader, Divider, Grid} from "@material-ui/core";
import {SearchInput} from "../../../../components";
import PropTypes from "prop-types";
import React from "react";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";


const useStyles = makeStyles(theme => ({
    root: {},
    row: {
        height: '42px',
        display: 'flex',
        alignItems: 'center',
        marginTop: theme.spacing(1)
    },
    spacer: {
        flexGrow: 1
    },
    importButton: {
        marginRight: theme.spacing(1)
    },
    searchInput: {
        marginRight: theme.spacing(1)
    }
}));


const modalStyle = makeStyles(() => ({
    modal: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}));


const MatcherToolbar = props => {
    const {setMatchers} = props;
    const [addMatcher, setAddMatcher] = React.useState({});
    const classes = useStyles();
    const modalClasses = modalStyle();

    const [modalState, setModalState] = React.useState(false);

    const handleModalState = (state) => {
        setModalState(state);
        setAddMatcher({});
    }

    const handleMatchersAdd = (newMatcher) => {
        fetch('http://localhost:8080/matcher/add', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newMatcher),
        }).then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Server response wasn\'t OK');
            }
        })
            .then((json) => {
                setMatchers(json.items);
            });
    }

    const updateAddValue = (e) => {
        const newAddMatcher = {
            matcherType: addMatcher.matcherType,
            name: addMatcher.name,
            description: addMatcher.description,
            expression: addMatcher.expression,
            trueCaseValue: addMatcher.trueCaseValue,
            falseCaseValue: addMatcher.falseCaseValue,
        }
        newAddMatcher[e.target.name] = e.target.value;
        setAddMatcher(newAddMatcher);
    }

    const handleAddAction = (newMatcher) => {
        handleMatchersAdd(newMatcher);
        handleModalState(false)
    }

    return (
        <div>
            <div>
                <Modal
                    open={modalState}
                    onClose={() => handleModalState(false)}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    className={clsx(modalClasses.modal)}
                >
                    <Grid
                        item
                        md={4}
                        xs={12}
                    >
                        <Card>
                            <form>
                                <CardHeader
                                    subheader="Add Employee matcher"
                                    title="Add matcher"
                                />
                                <CardContent>
                                    <TextField
                                        id="matcher-type"
                                        name="matcherType"
                                        label="Matcher Type"
                                        style={{margin: 8}}
                                        placeholder="Matcher Type"
                                        fullWidth
                                        margin="normal"
                                        value={addMatcher.matcherType}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={(e) => updateAddValue(e)}
                                    />
                                    <TextField
                                        id="matcher-name"
                                        name="name"
                                        label="Name"
                                        style={{margin: 8}}
                                        placeholder="Name"
                                        fullWidth
                                        margin="normal"
                                        value={addMatcher.name}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={(e) => updateAddValue(e)}
                                    />
                                    <TextField
                                        id="matcher-description"
                                        name="description"
                                        label="Description"
                                        style={{margin: 8}}
                                        placeholder="Description"
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        value={addMatcher.description}
                                        onChange={(e) => updateAddValue(e)}
                                    />
                                    <TextField
                                        id="matcher-expression"
                                        name="expression"
                                        label="Expression"
                                        style={{margin: 8}}
                                        placeholder="Expression"
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        value={addMatcher.expression}
                                        onChange={(e) => updateAddValue(e)}
                                    />
                                    <TextField
                                        id="matcher-true-case"
                                        name="trueCaseValue"
                                        label="True Case Value"
                                        style={{margin: 8}}
                                        placeholder="True Case Value"
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        value={addMatcher.trueCaseValue}
                                        onChange={(e) => updateAddValue(e)}
                                    />
                                    <TextField
                                        id="matcher-false-case"
                                        name="falseCaseValue"
                                        label="False Case Value"
                                        style={{margin: 8}}
                                        placeholder="False Case Value"
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        value={addMatcher.falseCaseValue}
                                        onChange={(e) => updateAddValue(e)}
                                    />
                                    <Divider/>
                                    <Button
                                        color="primary"
                                        variant="contained"
                                        onClick={() => handleAddAction(addMatcher)}
                                    >
                                        Save
                                    </Button>
                                </CardContent>
                            </form>
                        </Card>

                    </Grid>
                </Modal>
            </div>
            <div className={classes.row}>
                <span className={classes.spacer}/>
                <Button
                    color="primary"
                    variant="contained"
                    onClick={() => handleModalState(true)}
                >
                    Add matcher
                </Button>
            </div>
            <div className={classes.row}>
                <SearchInput
                    className={classes.searchInput}
                    placeholder="Search matcher"
                />
            </div>
        </div>
    );
};


MatcherToolbar.propTypes = {
    className: PropTypes.string
};

export default MatcherToolbar;
