import {makeStyles} from "@material-ui/styles";
import React, {useState} from "react";
import {
    Card,
    CardContent,
    CardHeader,
    Divider,
    Grid,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Typography
} from "@material-ui/core";
import clsx from "clsx";
import PerfectScrollbar from "react-perfect-scrollbar";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import {Delete, Edit} from "@material-ui/icons";
import Tooltip from "@material-ui/core/Tooltip";
import IconButton from "@material-ui/core/IconButton";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";

const useStyles = makeStyles(theme => ({
    root: {},
    content: {
        padding: 0
    },
    inner: {
        minWidth: 1050
    },
    nameContainer: {
        display: 'flex',
        alignItems: 'center'
    },
    avatar: {
        marginRight: theme.spacing(2)
    },
    actions: {
        justifyContent: 'flex-end'
    }
}));


const modalStyle = makeStyles(() => ({
    modal: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}));

const MatcherTable = props => {

    const {className, matchers, setMatchers} = props;
    const classes = useStyles();
    const [rowsPerPage] = useState(10);
    const [modalState, setModalState] = React.useState(false);
    const [editMatcher, setEditMatcher] = React.useState({});
    const [deleteMatcher, setDeleteMatcher] = React.useState({});
    const [open, setOpen] = React.useState(false);
    const modalClasses = modalStyle();

    const handleModalState = (state, matcher) => {
        setEditMatcher(matcher);
        setModalState(state);
    }

    const handleConfirmDialogEvent = (open, deleteMatcher) => {
        if (!open) {
            setDeleteMatcher({});
        } else {
            setDeleteMatcher(deleteMatcher);
        }
        setOpen(open);

    }
    const handleMatcherEdit = (editMatcher) => {
        const index = matchers.findIndex((match) => match.guid === editMatcher.guid);
        if (index >= 0) {
            const item = matchers[index];
            item.guid = editMatcher.guid;
            item.name = editMatcher.name;
            item.description = editMatcher.description;
            item.expression = editMatcher.expression;
            item.trueCaseValue = editMatcher.trueCaseValue;
            item.falseCaseValue = editMatcher.falseCaseValue;
            fetch('http://localhost:8080/matcher/edit', {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(item),
            }).then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Server response wasn\'t OK');
                }
            })
                .then((json) => {
                    setMatchers(json.items);
                });
        }
    }

    const handleEditAction = (editMatcher) => {
        handleMatcherEdit(editMatcher);
        handleModalState(false, {});
    }


    const handleMatcherDelete = (deleteMatcher) => {
        const index = matchers.findIndex((match) => match.guid === deleteMatcher.guid);
        if (index !== -1) {
            fetch('http://localhost:8080/matcher/remove/' + deleteMatcher.guid, {
                method: 'DELETE',
            }).then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Server response wasn\'t OK');
                }
            })
                .then((json) => {
                    setMatchers(json.items);
                });
        }
        handleConfirmDialogEvent(false);
    }
    const updateEditValue = (e) => {
        const newEditMatcher = {
            guid: editMatcher.guid,
            matcherType: editMatcher.matcherType,
            name: editMatcher.name,
            description: editMatcher.description,
            expression: editMatcher.expression,
            trueCaseValue: editMatcher.trueCaseValue,
            falseCaseValue: editMatcher.falseCaseValue,
        }
        newEditMatcher[e.target.name] = e.target.value;
        setEditMatcher(newEditMatcher);
    }

    return (
        <div>
            <div>
                <Modal
                    open={modalState}
                    onClose={() => handleModalState(false, {})}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description"
                    className={clsx(modalClasses.modal)}
                >
                    <Grid
                        item
                        md={4}
                        xs={12}
                    >
                        <Card>
                            <form>
                                <CardHeader
                                    subheader="Edit matcher"
                                    title="Edit matcher"
                                />
                                <CardContent>
                                    <TextField
                                        id="matcher-name"
                                        name="name"
                                        label="Name"
                                        style={{margin: 8}}
                                        placeholder="Name"
                                        fullWidth
                                        margin="normal"
                                        value={editMatcher.name}
                                        onChange={(e) => updateEditValue(e)}
                                    />
                                    <TextField
                                        id="matcher-description"
                                        name="description"
                                        label="Description"
                                        style={{margin: 8}}
                                        placeholder="Description"
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        value={editMatcher.description}
                                        onChange={(e) => updateEditValue(e)}
                                    />
                                    <TextField
                                        id="matcher-expression"
                                        name="expression"
                                        label="Expression"
                                        style={{margin: 8}}
                                        placeholder="Expression"
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        value={editMatcher.expression}
                                        onChange={(e) => updateEditValue(e)}
                                    />
                                    <TextField
                                        id="matcher-true-case"
                                        name="trueCaseValue"
                                        label="True Case Value"
                                        style={{margin: 8}}
                                        placeholder="True Case Value"
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        value={editMatcher.trueCaseValue}
                                        onChange={(e) => updateEditValue(e)}
                                    />
                                    <TextField
                                        id="matcher-false-case"
                                        name="falseCaseValue"
                                        label="False Case Value"
                                        style={{margin: 8}}
                                        placeholder="False Case Value"
                                        fullWidth
                                        margin="normal"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        value={editMatcher.falseCaseValue}
                                        onChange={(e) => updateEditValue(e)}
                                    />
                                    <Divider/>
                                    <Button
                                        color="primary"
                                        variant="contained"
                                        onClick={() => handleEditAction(editMatcher)}
                                    >
                                        Save
                                    </Button>
                                </CardContent>
                            </form>
                        </Card>

                    </Grid>
                </Modal>
            </div>

            <div>
                <Dialog
                    open={open}
                    onClose={() => handleConfirmDialogEvent(false)}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"Delete"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Are you sure you want to delete this item?
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => handleConfirmDialogEvent(false)} color="primary">
                            Disagree
                        </Button>
                        <Button onClick={() => handleMatcherDelete(deleteMatcher)} color="primary" autoFocus>
                            Agree
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
            <Card
                className={clsx(classes.root, className)}
            >
                <CardContent className={classes.content}>
                    <PerfectScrollbar>
                        <div className={classes.inner}>
                            <Table>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Type</TableCell>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Description</TableCell>
                                        <TableCell>Expression</TableCell>
                                        <TableCell>True Case</TableCell>
                                        <TableCell>False Case</TableCell>
                                        <TableCell>Actions</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {matchers.slice(0, rowsPerPage).map(matcher => (
                                        <TableRow
                                            className={classes.tableRow}
                                            hover
                                            key={matcher.guid}
                                        >
                                            <TableCell>
                                                <div className={classes.nameContainer}>
                                                    <Typography variant="body1">{matcher.matcherType}</Typography>
                                                </div>
                                            </TableCell>
                                            <TableCell>{matcher.name}</TableCell>
                                            <TableCell>{matcher.description}</TableCell>
                                            <TableCell>{matcher.expression}</TableCell>
                                            <TableCell>{matcher.trueCaseValue}</TableCell>
                                            <TableCell>{matcher.falseCaseValue}</TableCell>
                                            <TableCell>{
                                                <Tooltip title="Edit">
                                                    <IconButton aria-label="edit">
                                                        <Edit onClick={() => handleModalState(true, matcher)}/>
                                                    </IconButton>
                                                </Tooltip>
                                            } {
                                                <Tooltip title="Delete">
                                                    <IconButton aria-label="delete">
                                                        <Delete
                                                            onClick={() => handleConfirmDialogEvent(true, matcher)}/>
                                                    </IconButton>
                                                </Tooltip>
                                            }
                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </div>
                    </PerfectScrollbar>
                </CardContent>
            </Card>
        </div>
    );

}


MatcherTable.propTypes = {
    className: PropTypes.string,
    matchers: PropTypes.array.isRequired
};
export default MatcherTable;

