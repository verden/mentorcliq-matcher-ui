import {makeStyles} from "@material-ui/styles";
import React, {useEffect, useState} from "react";
import MatcherToolbar from "./components/MatcherToolbar";
import MatcherTable from "./components/MatcherTable";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3)
    },
    content: {
        marginTop: theme.spacing(2)
    }
}));


const MatcherList = () => {
    const classes = useStyles(undefined);

    const [matchers, setMatchers] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/matcher/items', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Server response wasn\'t OK');
            }
        })
            .then((json) => {
                setMatchers(json.items);
            });
    }, []);

    return (
        <div className={classes.root}>
            <MatcherToolbar setMatchers={setMatchers} matchers={matchers}/>
            <div className={classes.content}>
                <MatcherTable matchers={matchers} setMatchers={setMatchers}/>
            </div>
        </div>
    );
}

export default MatcherList;
