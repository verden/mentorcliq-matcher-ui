import uuid from 'uuid/v1';

export default [
    {
        id: uuid(),
        name: 'Gabrielle Clarkson',
        email: 'tamas@me_example.com',
        division: 'Accounting',
        age: 25,
        timezone: 2
    },
    {
        id: uuid(),
        name: 'Zoe Peters',
        email: 'gozer@icloud_example.com',
        division: 'Finance',
        age: 30,
        timezone: 3
    },
    {
        id: uuid(),
        name: 'Jacob Murray',
        email: 'lstein@me_example.com',
        division: 'Accounting',
        age: 22,
        timezone: 2
    },
    {
        id: uuid(),
        name: 'Nicholas Vance',
        email: 'saridder@outlook_example.com',
        division: 'Engineering',
        age: 27,
        timezone: -3
    },
    {
        id: uuid(),
        name: 'Jason Hamilton',
        email: 'osaru@live_example.com',
        division: 'HR',
        age: 35,
        timezone: 4
    },
    {
        id: uuid(),
        name: 'Sally Bower',
        email: 'bulletin@att_example.com',
        division: 'Engineering',
        age: 20,
        timezone: 10
    },
    {
        id: uuid(),
        name: 'Max Baker',
        email: 'kodeman@gmail_example.com',
        division: 'Engineering',
        age: 32,
        timezone: 0
    },
    {
        id: uuid(),
        name: 'Olivia Ogden',
        email: 'gator@outlook_example.com',
        division: 'Finance',
        age: 32,
        timezone: -4
    },
    {
        id: uuid(),
        name: 'Adrian Ince',
        email: 'delpino@comcast_example.com',
        division: 'Finance',
        age: 28,
        timezone: -5
    },
    {
        id: uuid(),
        name: 'William Clarkson',
        email: 'goldberg@hotmail_example.com',
        division: 'Accounting',
        age: 29,
        timezone: 3
    }
];
