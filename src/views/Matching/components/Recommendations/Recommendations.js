import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {makeStyles, withStyles} from '@material-ui/styles';
import {Card, CardContent, CardHeader, Divider, Typography} from '@material-ui/core';
import Box from "@material-ui/core/Box";

const useStyles = makeStyles(() => ({
    root: {},
    item: {
        display: 'flex',
        flexDirection: 'column'
    }
}));

const Recommendations = props => {
    const {className} = props;
    const [recommendations, setRecommendations] = React.useState([]);
    const classes = useStyles();

    useEffect(() => {
        fetch('http://localhost:8080/matching/recommendations', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Server response wasn\'t OK');
            }
        })
            .then((json) => {
                setRecommendations(json.items);
            });
    }, []);

    return (
        <Card
            className={clsx(classes.root, className)}
        >
            <CardHeader
                subheader="Matching Recommendations"
                title="Recommendations"
            />
            <Divider/>

            <CardContent>
                {recommendations.map(recommend => (
                    <div>
                        <Box component="div">
                            <BlueTextTypography variant="h4">
                                {recommend.firstMessage}
                            </BlueTextTypography>
                        </Box>
                        <Divider/>
                        <Box component="div">
                            <BlueTextTypography variant="h4">
                                {recommend.secondMessage}
                            </BlueTextTypography>
                        </Box>
                        <br/>
                        <Divider/>
                        <Box component="div">
                            <RedTextTypography variant="h6">
                                {recommend.scoreMessage}
                            </RedTextTypography>
                        </Box>
                        <br/>
                        <br/>
                    </div>
                ))}
            </CardContent>
            <Divider/>
        </Card>
    );
};

const BlueTextTypography = withStyles({
    root: {
        color: "blue"
    }
})(Typography);

const RedTextTypography = withStyles({
    root: {
        color: "red"
    }
})(Typography);

Recommendations.propTypes = {
    className: PropTypes.string
};

export default Recommendations;
