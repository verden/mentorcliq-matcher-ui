import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {
    Button,
    Card,
    CardActions,
    CardContent,
    CardHeader,
    Checkbox,
    Divider,
    FormControlLabel, Typography
} from "@material-ui/core";
import Box from "@material-ui/core/Box";
import {withStyles} from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 400,
    },
}));

const MatchingSection = () => {
    const classes = useStyles();
    const [a_emp, setA_emp] = React.useState('');
    const [b_emp, setB_emp] = React.useState('');
    const [matcher, setMatcher] = React.useState('');
    const [a_empError, setA_empError] = React.useState(false);
    const [b_empError, setB_empError] = React.useState(false);
    const [matcherError, setMatcherError] = React.useState(false);
    const [availableMatchers, setAvailableMatchers] = React.useState([]);
    const [checked, setChecked] = React.useState(false);
    const [matchResult, setMatchResult] = React.useState('');
    const [hidden, setHidden] =  React.useState('hidden');

    const handleMatchClick = () => {
        const matchRequest = {
            'firstEmployeeGuid': a_emp,
            'secondEmployeeGuid': b_emp,
        }
        if (checked) {
            matchRequest.matcherGuid = "*";
        } else {
            matchRequest.matcherGuid = matcher;
        }
        fetch('http://localhost:8080/matching/match', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(matchRequest),
        }).then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Server response wasn\'t OK');
            }
        })
            .then((json) => {
                setMatchResult(json.message);
                setHidden('visible');
            });
    }

    const validateSelectValues = () => {
        let hasAnyError = false;
        if (!a_emp) {
            setA_empError(true);
            hasAnyError = true;
        } else {
            if (a_emp) {
                setA_empError(false);
            }
        }
        if (!b_emp) {
            hasAnyError = true;
            setB_empError(true);
        } else {
            if (b_emp) {
                setB_empError(false);
            }
        }
        if (!matcher) {
            if (!checked) {
                setMatcherError(true);
                hasAnyError = true;
            } else {
                setMatcherError(false);
            }
        } else {
            if (matcher) {
                setMatcherError(false);
            }
        }
        setHidden('hidden');
        if (!hasAnyError) {
            handleMatchClick();
        }
    };

    const handleChange = (event) => {
        if (event.target.name === 'a_emp') {
            setA_emp(event.target.value);
            if (event.target.value) {
                setA_empError(false);
            }
        }
        if (event.target.name === 'b_emp') {
            setB_emp(event.target.value);
            if (event.target.value) {
                setB_empError(false);
            }
        }
        if (event.target.name === 'matcher') {
            setMatcher(event.target.value);
            setChecked(false);
            if (event.target.value) {
                setMatcherError(false);
            }
        }
        setHidden('hidden');
    };

    const [employees, setEmployees] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/matching/items', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Server response wasn\'t OK');
            }
        })
            .then((json) => {
                setEmployees(json.employees);
                setAvailableMatchers(json.matchers);
            });
    }, []);

    const handleAllCheckedValue = (event) => {
        if (event.target.checked) {
            setMatcher('');
        }
        setChecked(event.target.checked);
    };
    return (
        <Card>
            <form>
                <CardHeader
                    subheader="Select Employees for matching"
                    title="Employee matching"
                />
                <Divider/>
                <CardContent>
                    <FormControl className={classes.formControl}>
                        <InputLabel>Employee</InputLabel>
                        <Select
                            required
                            id="a_emp"
                            name="a_emp"
                            value={a_emp}
                            onChange={handleChange}
                            error={a_empError}
                        >
                            <MenuItem key={'a_emp_empty'}
                                      value=''/>
                            {employees.map(employee => (
                                <MenuItem key={'a_emp_' + employee.guid}
                                          value={employee.guid}>{employee.name}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <Divider/>
                    <FormControl className={classes.formControl}>
                        <InputLabel>Employee</InputLabel>
                        <Select
                            id="b_emp"
                            name="b_emp"
                            value={b_emp}
                            error={b_empError}
                            onChange={handleChange}
                        >
                            <MenuItem key={'b_emp_empty'}
                                      value=''/>
                            {employees.map(employee => (
                                <MenuItem key={'b_emp_' + employee.guid}
                                          value={employee.guid}>{employee.name}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <Divider/>
                    <FormControl className={classes.formControl}>
                        <InputLabel>Matchers</InputLabel>
                        <Select
                            required
                            id="matcher"
                            name="matcher"
                            value={matcher}
                            onChange={handleChange}
                            error={matcherError}
                        >
                            <MenuItem key={'matcher_empty'}
                                      value=''/>
                            {availableMatchers.map(matcher => (
                                <MenuItem key={'matcher_' + matcher.guid}
                                          value={matcher.guid}>{matcher.name}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <FormControl className={classes.formControl}>
                        <FormControlLabel
                            control={
                                <Checkbox
                                    color="primary"
                                    checked={checked}
                                    onChange={handleAllCheckedValue}
                                />
                            }
                            label="Match with all matchers"
                        />
                    </FormControl>

                    <Box component="div" visibility={hidden}>
                        <RedTextTypography variant="h5">
                            {matchResult}
                        </RedTextTypography>
                    </Box>
                </CardContent>
                <Divider/>
                <CardActions>
                    <Button
                        color="primary"
                        variant="outlined"
                        onClick={() => validateSelectValues()}
                    >
                        Match
                    </Button>
                </CardActions>
            </form>
        </Card>
    );
}

const RedTextTypography = withStyles({
    root: {
        color: "red"
    }
})(Typography);
export default MatchingSection;
