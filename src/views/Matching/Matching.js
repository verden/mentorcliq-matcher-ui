import React, {useState} from 'react';
import {makeStyles} from '@material-ui/styles';
import {Grid} from '@material-ui/core';

import {Recommendations} from './components';
import MatchingSection from "./components/MatchingSection";

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(4)
    }
}));

const Matching = () => {
    const classes = useStyles();

    const [status, setStatus] = useState('');

    const handleMatchClick = (event) => {
        console.log("Event : ", event);

        //back end call
        setStatus('test');
    };


    return (
        <div className={classes.root}>
            <Grid
                container
                spacing={4}
            >
                <Grid
                    item
                    md={4}
                    xs={12}
                >
                    <MatchingSection handleMatchClick={handleMatchClick}/>

                </Grid>
                <Grid
                    item
                    md={7}
                    xs={12}
                >
                    <Recommendations status={status}/>

                </Grid>
            </Grid>
        </div>
    );
};

export default Matching;
