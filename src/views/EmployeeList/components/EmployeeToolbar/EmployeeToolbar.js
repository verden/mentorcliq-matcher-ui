import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/styles';
import {Button} from '@material-ui/core';
import SearchInput from "../../../../components/SearchInput";


const useStyles = makeStyles(theme => ({
    root: {},
    row: {
        height: '42px',
        display: 'flex',
        alignItems: 'center',
        marginTop: theme.spacing(1)
    },
    spacer: {
        flexGrow: 1
    },
    importButton: {
        marginRight: theme.spacing(1)
    },
    searchInput: {
        marginRight: theme.spacing(1)
    }
}));

const EmployeeToolbar = props => {
    const {className, setEmployees} = props;
    const [selectedFile, setSelectedFile] = React.useState(null);
    const [disabled, setDisabled] = React.useState(true);
    const classes = useStyles();

    const uploadFile = () => {
        if (selectedFile) {
            const formData = new FormData();
            formData.append('file', selectedFile);
            fetch('http://localhost:8080/employee/upload', {
                method: 'POST',
                body: formData
            }).then((response) => {
                if (response.ok) {
                    return response.json();
                } else {
                    throw new Error('Server response wasn\'t OK');
                }
            })
                .then((json) => {
                    setEmployees(json.items);
                });
            setSelectedFile(null);
            setDisabled(true);
        }
    };


    const onFileChange = event => {
        if (event.target.files[0]) {
            setSelectedFile(event.target.files[0]);
            setDisabled(false);
        } else {
            setSelectedFile(null);
            setDisabled(true);
        }
    };
    return (
        <div
            className={clsx(classes.root, className)}
        >
            <div className={classes.row}>
                <span className={classes.spacer}/>
                <Button
                    variant="contained"
                    component="label"
                    className={classes.importButton}
                >
                    Import
                    <input
                        accept=".csv"
                        type="file"
                        style={{display: "none"}}
                        onChange={onFileChange}
                    />
                </Button>
                <Button
                    disabled={disabled}
                    color="primary"
                    variant="contained"
                    onClick={uploadFile}
                >
                    Upload
                </Button>
            </div>
            <div className={classes.row}>
                <SearchInput
                    className={classes.searchInput}
                    placeholder="Search employee"
                />
            </div>
        </div>
    );
};

EmployeeToolbar.propTypes = {
    className: PropTypes.string
};

export default EmployeeToolbar;
