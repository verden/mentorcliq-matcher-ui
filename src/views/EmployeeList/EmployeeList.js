import React, {useEffect, useState} from 'react';
import {makeStyles} from '@material-ui/styles';

import {EmployeeTable, EmployeeToolbar} from './components';

const useStyles = makeStyles(theme => ({
    root: {
        padding: theme.spacing(3)
    },
    content: {
        marginTop: theme.spacing(2)
    }
}));

const EmployeeList = () => {
    const classes = useStyles();
    const [employees, setEmployees] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8080/employee/items', {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
            },
        }).then((response) => {
            if (response.ok) {
                return response.json();
            } else {
                throw new Error('Server response wasn\'t OK');
            }
        })
            .then((json) => {
                setEmployees(json.items);
            });
    }, []);
    return (
        <div className={classes.root}>
            <EmployeeToolbar setEmployees={setEmployees}/>
            <div className={classes.content}>
                <EmployeeTable employees={employees}/>
            </div>
        </div>
    );
};

export default EmployeeList;
